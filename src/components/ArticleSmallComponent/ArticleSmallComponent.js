/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { Link } from 'react-router-dom';
import { ArticleService } from '../../services/article.service';
import { BaseComponent } from '../_base-component';
import './ArticleSmallComponent.scss';

export class ArticleSmallComponent extends BaseComponent {
    constructor() {
        super();
        this.state = {
            isAdmin: false,
        }
    }

    async delete(id) {
        const response = await ArticleService.deleteArticle(id);
        window.location.reload(false);
    }

    async componentDidMount() {
        this.setState({isAdmin: this.props.isAdmin});
    }

    render() {
        const article = this.props.article;
        console.log(article);
        return (
            <div className="card-container col-12 col-md-6 col-lg-3">
                <div className="card card--articles shadow">
                    <div className="row g-0">
                        <div className="col-md-12 card-header-img card-header-img--articles">
                            <img src={article.picture} alt="" />
                        </div>

                        <div className="col-md-12 d-flex justify-content-center">
                            <div className="card-body overflow">
                                <h5 className="card-title">{article.title}</h5>
                                <p className="card-text line-clamp">{article.content}</p>
                            </div>
                        </div>
                        <Link className="btn btn-primary btn-articles" to={{ pathname: '/articles/' + article.id }}>En savoir plus</Link>
                    </div>
                </div>
                {this.state.isAdmin ?
                    <div>
                        <Link className="btn btn-warning btn-articles" to={{ pathname: '/admin/articles/' + article.id }}>Modifier</Link>
                        <button className="btn btn-danger btn-articles" onClick={() => this.delete(article.id)}>Supprimer</button>
                    </div>
                    : ''}
            </div>
        );
    }
}