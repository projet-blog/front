import './App.scss';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ExemplePage from '../exemple/ExemplePage';
import HomePage from '../home/HomePage';
import ArticlePage from '../article/ArticlePage';
import AdminListArticlesPage from '../admin/list-articles/AdminListArticlesPage';
import AdminEditArticlePage from '../admin/edit-article/AdminEditArticlePage';


function App() {
  return (
    <Router>
      <Switch className="container">
        <Route path="/exemple" component={ExemplePage} />
        <Route path="/articles/:id" component={ArticlePage} />
        <Route path="/admin/articles/:id" component={AdminEditArticlePage} />
        <Route path="/admin/articles" component={AdminListArticlesPage} />

        {/* Last page */}
        <Route path="/" component={HomePage} />
      </Switch>
    </Router>
  );
}

export default App;
