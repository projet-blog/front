import './AdminEditArticlePage.scss';
import { BasePage } from '../../_base-page';
import { ArticleService } from '../../../services/article.service';
import { Redirect } from "react-router-dom";


class AdminEditArticlePage extends BasePage {
    id;
    constructor() {
        super();
        this.state = {
            article: {},
            redirect: null,
        }
        this.handleChange = this.handleChange.bind(this);
        this.validate = this.validate.bind(this);

    }

    handleChange(e) {
        const name = e.target.name;
        let article = this.state.article;
        article[name] = e.target.value;
        this.setState({
            article: article
        })
    }

    async getArticle(id) {
        let result = await ArticleService.getArticle(id);

        if (result.status !== 200) {
            console.log('erreur : ' + result.statusText);
        } else {
            this.setState({ article: result.data});
        }
    }

    async validate(e) {
        e.preventDefault();
        let result;
        console.log(this.state.article);
        if(this.id !== 'new') {
            result = await ArticleService.updateArticle(this.id, this.state.article);
        }
        else {
            result = await ArticleService.createArticle(this.state.article);
        }
        const route = "/admin/articles";
        this.setState({ redirect: route });
    }

    async componentDidMount() {
        const { match: { params } } = this.props;
        this.id = params.id;
        if(this.id !== 'new')
            await this.getArticle(this.id);
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        return (
            <div className="AdminEditArticlePage">
                <h1>AdminEditArticlePage Page</h1>
                <form className="container article-page__form" onSubmit={this.validate}>
                    <div>
                        <label>
                            Titre
                        </label>
                        <input name="title" type="text" value={this.state.article?.title} required onChange={this.handleChange}/>
                    </div>
                    <div>
                        <label>
                            Image
                        </label>
                        <input name="picture" type="text" value={this.state.article?.picture} required onChange={this.handleChange}/>
                    </div>
                    <div>
                        <label>
                            Contenu
                        </label>
                        <textarea cols="50" rows="10" name="content" value={this.state.article?.content} required onChange={this.handleChange}></textarea>
                    </div>
                    {/* <a className="btn btn-secondary" onClick={() => this.handleClick(undefined)}>Annuler</a> */}
                    <button className="btn btn-primary" type="submit">Valider</button>
                </form>
            </div>
        );
    }
}

export default AdminEditArticlePage;