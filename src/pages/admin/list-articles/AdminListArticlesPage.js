import './AdminListArticlesPage.scss';
import { BasePage } from '../../_base-page';
import { ArticleSmallComponent } from '../../../components/ArticleSmallComponent/ArticleSmallComponent';
import { ArticleService } from '../../../services/article.service';
import { Link } from 'react-router-dom';

class AdminListArticlesPage extends BasePage {
    constructor() {
        super();
        this.state = {
            articles: []
        }
    }
    async getArticles() {
        let articlesHtml = [];
        let result = await ArticleService.getAllArticles();

        if (result.status !== 200) {
            console.log('erreur : ' + result.statusText);
        } else {
            for (const data of result.data) {
                articlesHtml.push(<ArticleSmallComponent article={data} isAdmin={true}></ArticleSmallComponent>);
            }
            this.setState({ articles: articlesHtml});
        }
    }

    async componentDidMount() {
        await this.getArticles();
    }

    render() {
        return (
            <div className="position-relative">
                <div className="AdminListArticlesPage">
                    <h1>AdminListArticlesPage</h1>
                    <Link className="btn btn-primary btn-articles" to={{ pathname: '/' }}>Accueil</Link>
                    <div>
                        {/* <p className="home-sous-titre">Articles</p> */}
                        <div className="row">
                            {this.state.articles}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminListArticlesPage;