import './ArticlePage.scss';
import { BasePage } from '../_base-page';
import { ArticleService } from '../../services/article.service';
import { Link } from 'react-router-dom';

class ArticlePage extends BasePage {
    constructor() {
        super();
        this.state = {
            article: {}
        }
    }

    async getArticle(id) {
        let result = await ArticleService.getArticle(id);

        if (result.status !== 200) {
            console.log('erreur : ' + result.statusText);
        } else {
            this.setState({ article: result.data});
        }
    }

    async componentDidMount() {
        const { match: { params } } = this.props;
        this.id = params.id;
        await this.getArticle(this.id);
    }

    render() {
        return (
            <div className="ArticlePage">
                <h1>ArticlePage</h1>
                <Link className="btn btn-primary btn-articles" to={{ pathname: '/' }}>Accueil</Link>
                <div className="card card--articles shadow">
                    <div className="row g-0">
                        <div className="col-md-12 card-header-img card-header-img--articles">
                            <img src={this.state.article?.picture} alt="" />
                        </div>

                        <div className="col-md-12 d-flex justify-content-center">
                            <div className="card-body overflow">
                                <h5 className="card-title">{this.state.article?.title}</h5>
                                <p className="card-text">{this.state.article?.content}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default ArticlePage;