import './ExemplePage.scss';
import { ExempleService } from '../../services/exemple.service';
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';

class ExemplePage extends BasePage {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="ExemplePage">
                <h1>Exemple Page</h1>

                {/* <Link to={Routes.Home}>Go back</Link> */}
                <button onClick={ExempleService.exempleMethod}>Exemple service : Log</button>
                {/* <Link className="btn btn-primary" to={{ pathname: Routes.Programmation, data: { location: 'all' } }}>Bourges</Link> */}

            </div>
        );
    }
}

export default ExemplePage;