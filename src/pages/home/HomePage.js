import './HomePage.scss';
import { BasePage } from '../_base-page';
import { ArticleService } from '../../services/article.service';
import { ArticleSmallComponent } from '../../components/ArticleSmallComponent/ArticleSmallComponent';
import { Link } from 'react-router-dom';

class HomePage extends BasePage {
    article = {
        title: "Test de création",
        content: "Nulla et quam vehicula, feugiat tellus ut, semper ex. In eleifend mi eget libero fermentum aliquet. Quisque tortor justo, pellentesque in nibh eu, cursus rhoncus dui. Sed vitae tempus diam. Integer maximus urna mauris, et pretium diam molestie et. Praesent congue porta elementum. Sed eget elementum sem. Maecenas cursus odio in ullamcorper scelerisque. Duis eget accumsan est. Integer venenatis diam eu consectetur sollicitudin. Nullam id posuere nisi, id elementum nulla.",
        dateCreation: "2020-02-11 13:13:00"
    }

    article2 = {
        title: "Test de modification",
    }

    constructor() {
        super();
        this.state = {
            articles: []
        }
    }

    async getArticles() {
        // this.setState({ loaded: false });
        let articlesHtml = [];
        let result = await ArticleService.getAllArticles();

        if (result.status !== 200) {
            console.log('erreur : ' + result.statusText);
        } else {
            for (const data of result.data) {
                articlesHtml.push(<ArticleSmallComponent article={data}></ArticleSmallComponent>);
            }
            this.setState({ articles: articlesHtml});
        }
    }

    async componentDidMount() {
        await this.getArticles();
    }

    render() {
        return (
            <div className="position-relative">
                <div className="HomePage">
                    <h1>HomePage</h1>
                    <Link className="btn btn-primary btn-articles" to={{ pathname: '/admin/articles' }}>Admin</Link>

                    <div>
                        {/* <p className="home-sous-titre">Articles</p> */}
                        <div className="row">
                            {this.state.articles}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HomePage;