import axios from 'axios';

const api = 'http://localhost:8000/api';

export class ArticleService {
    static async getAllArticles() {
        console.log('getAllArticles()');
        const url = api + '/articles';
        const response = await axios.get(url);
        console.log('response');
        console.log(response);
        return response;
    }

    static async getArticle(id) {
        console.log('getArticle(' + id + ')');
        const url = api + '/articles/' + id;
        const response = await axios.get(url);
        console.log('response');
        console.log(response);
        return response;
    }

    static async createArticle(article) {
        console.log('createArticle()');
        article.dateCreation = new Date();
        console.log(article);
        const url = api + '/articles';
        const response = await axios.post(url, article, {
            headers: {
                'Accept': 'application/json',
            },
        });
        console.log('response');
        console.log(response);
        return response;
    }

    static async updateArticle(id, article) {
        console.log('updateArticle()');
        console.log(article);
        const url = api + '/articles/' + id;
        const response = await axios.put(url, article, {
            headers: {
                'Accept': 'application/json',
            },
        });
        console.log('response');
        console.log(response);
        return response;
    }

    static async deleteArticle(id) {
        console.log('deleteArticle()');
        console.log(id);
        const url = api + '/articles/' + id;
        const response = await axios.delete(url);
        console.log('response');
        console.log(response);
        return response;
    }
}